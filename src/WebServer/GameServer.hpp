/*
 * GameServer.h
 *
 *  Created on: 17 kwi 2017
 *      Author: user
 */

#ifndef GAMESERVER_H_
#define GAMESERVER_H_

//class Client;

#include <iostream>
#include <string>
#include <ctime>
#include "map"
#include "queue"

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>
#include "json.hpp"
#include "ProtocolElements/ProtocolParser.hpp"
#include "ProtocolElements/libwshandshake.hpp"
//#include "Game/EmpireGame.hpp"

using namespace boost;

using json=nlohmann::json;

enum state{
	newGame,
	waitingPlayers,
	ready,
	started,
	Game_Prepared,//clienci narysowali plansze wysylamy karty
	Send_Bet,//karty otzymane, wyslac licytacje
	Game_Init,//wybrano pierwszego gracza -> rozpoczecie normalnej gry:
	Game_Ongoing,
	finished
};



template <class TGame, class TPlayer>
class GameServer: public boost::enable_shared_from_this<GameServer<TGame,TPlayer>> {
public:

	class Client: public boost::enable_shared_from_this<Client> {
		public:
			Client(boost::asio::io_service& io_service,GameServer<TGame,TPlayer>* server);
			virtual ~Client();

			boost::asio::ip::tcp::socket& socket();
			void start();

			void handle_read(const boost::system::error_code& error,size_t bytes_transferred);
			void handle_write(const boost::system::error_code& error, size_t bytes);

			int setId(int & Id);
			int getId();
			std::string getName();
			int setName(std::string);

			int recogniseHandshake(std::string *message);
			void WebsocketHandshake(const boost::system::error_code& error,size_t bytes_transferred);

			std::string decodeData(std::string *msg,int &error);
			std::string encodeData(std::string *msg);

			void sendJson(json,bool);
			void sendMsg(std::string msg);
			void messageSent( const boost::system::error_code & err, size_t bytes);
			void read();

			void updatePlayers();
			void updateGames();
			void updateAll();
			void updateAllEach();

			int getStatus();
			int getRead();

			bool isReady();
			void setReady(bool ready);
			void attachPlayer(boost::shared_ptr<TPlayer>player);

			boost::shared_ptr<TPlayer> getPlayer();
			boost::shared_ptr<TGame> getGame();

			int detachGame();

			int addtoQueue(json j);
			void sendQueue(bool repeat);
			void sentFromQueue( const boost::system::error_code & err, size_t bytes);

		private:
			volatile bool _read;
			GameServer* _pServer;
			boost::asio::ip::tcp::socket _socket;
			enum { _maxLength = 1024 };
			char _data[_maxLength];
			int _Id;
			std::string _name;
			ProtocolParser _protocloParser;
			boost::shared_ptr<TGame> _gra;
			std::string response;
			int _status;
			bool _deleted=false;
			bool _ready=false;
			boost::shared_ptr<TPlayer> _player;
			std::queue<json> _kolejkaWiadomosci;
		};

	GameServer(boost::asio::io_service& io_service, short port);
	virtual ~GameServer();
	json getClients();
	int getNrClients();
	json getGames();
	int createGame(int owner,int players,int mapa, std::string nazwa);
	boost::shared_ptr<TGame> getGame(int);

	void addToAllertQueue(json);
	void allertQueue();
	void allertPlayers(json);

	void clearPlayers();
	void clearGames();




protected:
	void start_accept();
	 void handle_accept(boost::shared_ptr<Client> new_session,const boost::system::error_code& error);

private:
	enum {_maxClients = 50};
	boost::asio::io_service& _io_service;
	boost::asio::ip::tcp::acceptor _acceptor;
	std::map<int,boost::shared_ptr<Client>> _ClientList;
	std::map<int,boost::shared_ptr<TGame>>_GameList;
};
/*
 *  server implementation
 */

template <class TGame, class TPlayer> GameServer <TGame,TPlayer>::GameServer(boost::asio::io_service& io_service, short port):_io_service(io_service),
_acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
	start_accept();
}

template <class TGame, class TPlayer> GameServer <TGame,TPlayer>::~GameServer() {
	// TODO Auto-generated destructor stub
}

template <class TGame, class TPlayer> void GameServer <TGame,TPlayer>::start_accept()
{
	boost::shared_ptr<GameServer<TGame,TPlayer>::Client> newClient ( new GameServer<TGame,TPlayer>::Client(_io_service,this));
	    _acceptor.async_accept(newClient->socket(),
	        boost::bind(&GameServer<TGame,TPlayer>::handle_accept, this, newClient,boost::asio::placeholders::error));
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::handle_accept(boost::shared_ptr<Client> newClient,const boost::system::error_code& error)
{
	int id;
	std::string msg;
	if (!error)
	{
		//clearPlayers();
		if(_ClientList.size()<=_maxClients)
		{
			do
			{
				id = random();

			}while(_ClientList.find(id)!=_ClientList.end());
			//std::cout<<" connection id: "<<id<<std::endl;
			_ClientList.insert(std::pair<int,boost::shared_ptr<Client>>(id,newClient));
			newClient->setId(id);
			newClient->start();
		}
		else
		{
			//newClient.stop();
			newClient.reset();
		}
	}
	else
	{
		//delete newClient;
		newClient.reset();
	}

	start_accept();
}

template <class TGame, class TPlayer> json GameServer<TGame,TPlayer>::getClients()
{
	json k;
	try
	{
		for(auto it:this->_ClientList)
		{
			json j;
			j["id"]=it.second->getId();
			j["name"] = it.second->getName();
			k+=j;
		}
	}
	catch(std::exception e)
	{
		std::cout<<"except "<<e.what()<<std::endl;
	}
	return k;
}

template <class TGame, class TPlayer>json GameServer<TGame,TPlayer>::getGames()
{
	json k;
	for(auto it:this->_GameList)
	{
		json j;
		j["id"]=it.first;//->getId();
		j["name"] = it.second->getName();
		j["state"] = it.second->getState();
		k+=j;
	}
	return k;
}

template <class TGame, class TPlayer>int GameServer<TGame,TPlayer>::createGame( int owner,int players,int mapa,std::string nazwa)
{
	int id;
	do
	{
		id = random();

	}while(_GameList.find(id)!=_GameList.end());
	boost::shared_ptr<TGame> game(new TGame(this->_ClientList[owner],id,players,mapa,nazwa));
	_GameList.insert(std::pair<int,boost::shared_ptr<TGame>>(id,game));

	return id;
}

template <class TGame, class TPlayer> boost::shared_ptr<TGame> GameServer<TGame,TPlayer>::getGame(int gameId)
{
	if(_GameList.find(gameId)!=_GameList.end())
		return _GameList[gameId];
	else
		return nullptr;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::allertPlayers(json j)
{
	for(auto it:this->_ClientList)
	{
		//std::cout<<"alerted "<<it.first<<" "<<std::endl;
		if(it.second->getStatus())
			it.second->sendJson(j,false);
	}
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::getNrClients()
{
	return _ClientList.size();
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::clearPlayers()
{
	json j;
	typename std::map<int,boost::shared_ptr<GameServer<TGame,TPlayer>::Client> >::iterator it;
	for(it= _ClientList.begin();it!=_ClientList.end();it++)
	{
		if(!_ClientList.at(it->first)->getStatus())
		{
			//if(it->second.use_count())
			int key = it->first;
			try
			{
				//std::cout<<"reset client"<<key<<" count: "<<it->second.use_count()<<" "<<it->second->socket().is_open()<<std::endl;
				//std::cout<<"clients: "<< _ClientList.size()<<std::endl;

				_ClientList.at(it->first)->detachGame();

				_ClientList.erase(key);
				//std::cout<<"clients: "<<_ClientList.size()<<std::endl;
			}
			catch(std::exception e)
			{
				std::cout<<"exception" <<e.what()<<std::endl;
			}
			break;
		}
	}
	j["Type"]="Players";
	j["data"]=this->getClients();
	this->allertPlayers(j);
}
template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::clearGames()
{
	//bool changed= false;
	typename std::map<int,boost::shared_ptr<TGame> >::iterator it;
		for(it= _GameList.begin();it!=_GameList.end();it++)
		{
			if(_GameList.at(it->first)->getState()==state::finished)
			{
				//if(it->second.use_count())
				int key = it->first;
				try
				{
					if(it->second.use_count()==1)
					{
						//std::cout<<"reset game "<<key<<" count: "<<it->second.use_count()<<" "<<std::endl;
						//std::cout<<"server gier: "<<_GameList.size()<<std::endl;
						_GameList.erase(key);
						//std::cout<<"server gier: "<<_GameList.size()<<std::endl;
				//		changed = true;
					}

				}
				catch(std::exception e)
				{
					std::cout<<"exception" <<e.what()<<std::endl;
				}
				break;
			}
		}
		/*if(changed)
			this->allertPlayers(this->getGames());
*/
	}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::addToAllertQueue(json j)
{
	for(auto it: _ClientList)
	{
		it.second->addtoQueue(j);
	}
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::allertQueue()
{
	for(auto it: _ClientList)
	{
		it.second->sendQueue(false);
	}
}

/*
 * client of server (connection) implementation
 */


template <class TGame, class TPlayer> GameServer<TGame,TPlayer>::Client::Client(boost::asio::io_service& io_service,GameServer*  server):_socket(io_service),_protocloParser(),_Id(-1),_pServer(server),_read(false),_status(1) {
	// TODO Auto-generated constructor stub

}

template <class TGame, class TPlayer> GameServer<TGame,TPlayer>::Client::~Client() {
	// TODO Auto-generated destructor stub
	//std::cout<<"destructor klienta"<<_Id<<" "<<std::endl;
}

template <class TGame, class TPlayer> asio::ip::tcp::socket&  GameServer<TGame,TPlayer>::Client::socket()
{
	return _socket;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::handle_read(const boost::system::error_code& error,size_t bytes_transferred)
{
	_read = false;
	if (!error)
	{
		_status =1;
		int error;
		json jread,jwrite;
		std::string msg(_data,bytes_transferred);

		if(bytes_transferred<9)
		{
			/*
			 * delete
			 */
			boost::system::error_code err;
			_socket.cancel(err);
			std::cout<<err.message()<<std::endl;
			_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, err);
			_socket.close();
			//std::cout<<"id: "<<_Id<<" bytes: "<<bytes_transferred<<" read: ";
			for(int i=0;i<bytes_transferred;++i)
			{
				//std::cout<<_data[i];
			}
			_status=0;
			//std::cout<<"connection close "<<std::endl;

			_pServer->clearPlayers();
			return;
		}

		msg = decodeData(&msg,error);
		if(error!=system::errc::success)
		{
			//std::cout<<"decode error"<<std::endl;
			jwrite["Type"]="rep";
			jwrite["data"]="?";
			sendJson(jwrite,false);
			return;
		}
		else
		{
			try
			{
				jread=json::parse(msg);
			}
			catch(std::exception e)
			{
				//std::cout<<"id: "<<_Id<<" parsing exception "<<e.what()<<std::endl;
				//prosba o ponowne wyslanie
				jwrite["Type"]="rep";
				jwrite["data"]="?";
				sendJson(jwrite,false);
				return;
			}
			//std::cout<<"Id: "<<_Id<<" "<<jread<<std::endl;
			/*
			 * tutaj parsujemy typy i wybieramy dzialanie;
			 */
			if(jread.find("Type")==jread.end())
			{
				//std::cout<<"Id: "<<_Id<<" read error"<<std::endl;
					_status=0;
				//std::cout<<"connection close";
				//std::cout<<std::endl;

// wyslij informacje "break" do wszystkich graczy oprocz id;
				//_Id;
				//_gra;

				_pServer->clearPlayers();
				return;
			}
			if(jread["Type"] == "HI")
			{
				if(jread.find("data")!=jread.end())
				{
					setName(jread["data"]);
					std::cout<<"id: "<<_Id<<" "<<_name<<std::endl;
					jwrite.clear();
					jwrite["Type"]="id";
					jwrite["data"]=_Id;
					//sendJson(jwrite,false);
					addtoQueue(jwrite);
					//updateAll();
					updatePlayers();
				}
				else
				{
					//std::cout<<"Id: "<<_Id<<" notfound"<<std::endl;
				}
			}
			else if(jread["Type"] == "Id_Ok")
			{
				updateAllEach();
				//updatePlayers();
			}
			else if(jread["Type"] == "Players")
			{
				json jwrite;
				jwrite["Type"]="Players";
				jwrite["data"]=this->_pServer->getClients();
				addtoQueue(jwrite);
				sendQueue(false);
				//updatePlayers();
			}
			else if(jread["Type"] == "Games")
			{
				json jwrite;
				jwrite["Type"]="Games";
				jwrite["data"]=this->_pServer->getGames();
				addtoQueue(jwrite);
				sendQueue(false);
				//updateGames();
			}
			else if(jread["Type"]=="Init")
			{
				updateAll();
			}
			else if(jread["Type"] == "newGame")
			{
				if(jread.find("data")!=jread.end())
				{
					int mapa = atoi(jread["data"]["mapa"].get<std::string>().c_str());
					int players = atoi(jread ["data"]["players"].get<std::string>().c_str());
					std::string nazwa = jread ["data"]["nazwa"].get<std::string>();
					int graId = _pServer->createGame(this->_Id,players,mapa,nazwa);
					_gra=_pServer->getGame(graId);
					std::cout<<"stworzono nowa gre: "<<graId<<std::endl;
					setReady(false);

					updateGames();

					read();
				}
			}
			else if(jread["Type"]=="Join")
			{
				if(jread.find("data")!=jread.end())
				{
					int id=jread["data"].get<int>();
					if(_pServer->getGame(id)->getState()==1)
					{
						_gra = _pServer->getGame(id);
						_gra->addPlayer(shared_from_this());
						setReady(false);
					}
				}
			}
			else if(jread["Type"]=="Clear")
			{
				_status=0;
				//std::cout<<"id: "<<_Id<<" gra: ";
				//std::cout<<_gra.use_count()<<std::endl;
				if(_gra.use_count()>0)
				{
					_gra->setState(state::finished);
					detachGame();
					read();
				}
				else
				{
					read();
				}
			}
			else
			{
				if(_gra.use_count()>0)
				{
					jread["sId"]=_Id;
					_gra->execute(jread);
					if(_gra->getState()==state::finished)
					{
						//int graId = _pServer->createGame(this->_Id,players,mapa,nazwa);
						_pServer->clearGames();
						//_gra->clearGame();
					}
				}
			}
		}
	}
	else
	{
		std::string msg(_data,bytes_transferred);
		//std::cout<<"read error id: "<<_Id<<" "<<error.message()<< std::endl;
		boost::system::error_code err;
		_socket.cancel(err);
		//std::cout<<"socked canceled with: "<<_Id<<" "<<err.message()<< std::endl;
		//std::cout<<msg<<std::endl;
		//read();
		_socket.close();
		_status=0;
		//delete this;
	}
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::handle_write(const boost::system::error_code& error, size_t bytes)
{
	if (!error)
	{
		//std::cout<<"handle_write - id: "<<_Id<<" "<<bytes<<std::endl;
		read();
	}
	else
	{
		//std::cout<<"id: "<<this->_Id<<" write error"<<std::endl;
		_socket.close();
		delete this;
	}
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::WebsocketHandshake(const boost::system::error_code& error,size_t bytes_transferred)
{
	if (!error)
	{
		int error;
		std::string msg(_data,bytes_transferred);

		if(recogniseHandshake(&msg))
		{
			char acceptKey[29]={};
			std::string response = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: ";
			std::string key;

			_protocloParser.parseMessage(msg);
			for(auto it:_protocloParser.find("Sec-WebSocket-Key"))
			{
				key = it.substr(1,it.size());
			}
			WebSocketHandshake::generate(key.c_str(),acceptKey);

			response.append(acceptKey);
			response.append("\r\n\r\n");

			/*
			 * send handshake;
			 */
			sendMsg(response);
			std::cout<<"handshake succesfull"<<std::endl;
			/*
			 *  start normal session;
			 */
		}
		else
		{
			_status =0;
			//std::cout<<"Id: "<<_Id<<" handshake error"<<std::endl;
			_socket.close();
			delete this;
		}
	}
	else
	{
	  delete this;
	}
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::recogniseHandshake(std::string *message)
{
	if(message->find("GET")!=-1)
		if(message->find("Sec-WebSocket-Key")!=-1)
			return true;

	return false;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::start()
{
	_socket.async_read_some(boost::asio::buffer(_data, _maxLength),boost::bind(&Client::WebsocketHandshake, shared_from_this(),
	          boost::asio::placeholders::error,boost::asio::placeholders::bytes_transferred));
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::setId(int & Id)
{
	this->_Id=Id;
	return 0;
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::getId()
{
	return this->_Id;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::sendMsg(std::string msg)
{

	memcpy(_data,msg.c_str(),msg.size());
	//std::cout<<"Size "<<msg.size()<<std::endl	;
	_socket.write_some(boost::asio::buffer(msg.c_str(), msg.size()));
	read();
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::read()
{
	if(!_read)
	{
		//std::cout<<"id: "<<_Id<<" read"<<std::endl;
		try
		{
			_socket.async_read_some(boost::asio::buffer(_data, _maxLength),boost::bind(&Client::handle_read, shared_from_this(),_1,_2));
		}
		catch(std::exception& e)
		{
			std::cout<<"async_read_some exception: "<<e.what()<<std::endl;
		}
		_read = true;
	}
				//  boost::asio::placeholders::error,boost::asio::placeholders::bytes_transferred));
}

template <class TGame, class TPlayer> std::string GameServer<TGame,TPlayer>::Client::decodeData(std::string *message,int &error)
{
	std::string decoded;
	std::string masks;
	int lengthBit;
	uint64_t length;
	char indexFirstDataByte;

	char secondByte = message->at(1);
	length = lengthBit = secondByte & 127; // may not be the actual length in the two special cases

	char indexFirstMask = 2;          // if not a special case
	if (lengthBit == 126)           // if a special case, change indexFirstMask
	{
		indexFirstMask = 4;

		length = message->at(2)<<8;
		length = length|message->at(3);
	}
	else if( lengthBit == 127)       // ditto
	{
		indexFirstMask = 10;
		length = message->at(2)<56;
		length = length|message->at(3)<<48;
		length = length|message->at(4)<<40;
		length = length|message->at(5)<<32;
		length = length|message->at(6)<<24;
		length = length|message->at(7)<<16;
		length = length|message->at(8)<<8;
		length = length|message->at(9);
	}

	masks = message->substr(indexFirstMask,4);// four bytes starting from indexFirstMask

	indexFirstDataByte = indexFirstMask + 4; // four bytes further

	//std::cout<<"len: "<<length<<"firstbyte "<<indexFirstDataByte<<" "<<masks<<std::endl;
	int totalLength = indexFirstDataByte+length;

	if((totalLength-indexFirstDataByte)>message->size())
	{
		//std::cout<<"decoding error";
		error = -1;

		return "";
	}
	else
	{
		error =0;
	}

	try{
		for (int i = indexFirstDataByte,j=0; i < totalLength; ++i,++j)
		{
			decoded+= (message->at(i) ^ masks[j%4]);
		}
	}
	catch(std::out_of_range)
	{
				//std::cout<<std::endl<<std::endl<<std::endl<<totalLength-indexFirstDataByte <<" "<<message->size()<<" !!!! BROKEN MESSAGE !!!!"<<std::endl<<std::endl<<std::endl;
				error = -1;
				return  "";
	}
	return decoded;
}

template <class TGame, class TPlayer> std::string GameServer<TGame,TPlayer>::Client::encodeData(std::string *message)
{
	std::string response;
	int indexStartRawData = -1;
	char bytesFormatted[10];

	bytesFormatted[0] = 129; //fin, opcode - text

	if (message->size() <= 125)
	{
		bytesFormatted[1] = message->size();
		indexStartRawData = 2;
	}
	else if (message->size() >= 126 and message->size() <= 65535)
	{
		bytesFormatted[1] = 126;
		bytesFormatted[2] = ( message->size() >> 8 ) & 255;
		bytesFormatted[3] = ( message->size()      ) & 255;

		indexStartRawData = 4;
	}
	else
	{
		bytesFormatted[1] = 127;

		bytesFormatted[2] = ( message->size() >> 56 ) & 255;
		bytesFormatted[3] = ( message->size() >> 48 ) & 255;
		bytesFormatted[4] = ( message->size() >> 40 ) & 255;
		bytesFormatted[5] = ( message->size() >> 32 ) & 255;
		bytesFormatted[6] = ( message->size() >> 24 ) & 255;
		bytesFormatted[7] = ( message->size() >> 16 ) & 255;
		bytesFormatted[8] = ( message->size() >>  8 ) & 255;
		bytesFormatted[9] = ( message->size()       ) & 255;

		indexStartRawData = 10;
	}

	for(int i=0;i<indexStartRawData;++i)
	{
		response+=bytesFormatted[i];
	}
	response.append(*message);

	return response;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::sendJson(json j,bool again)
{
	if(!again)
	{
		response.clear();
		std::stringstream ss;
		boost::system::error_code err;

		j>>ss;
		response = ss.str();
		response = encodeData(&response);

		//send message;
		//std::cout<<"sendJson id: "<<_Id<<" "<<response<<std::endl;
	}
	else
	{
		//std::cout<<"again sendJson id: "<<_Id<<" "<<response<<std::endl;

	}
	try
	{
		_socket.async_send(asio::buffer(response.c_str(),response.size()),boost::bind(&Client::messageSent,shared_from_this(),_1,_2));
	}
	catch(std::exception& e)
	{
		//std::cout<<"async_write exception: "<<e.what()<<std::endl;
	}
}

template <class TGame, class TPlayer> std::string GameServer<TGame,TPlayer>::Client::getName()
{
	return this->_name;
}
template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::setName(std::string name)
{
	this->_name = name;
	return 0;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::updatePlayers()
{
	json jwrite;
	jwrite["Type"]="Players";
	jwrite["data"]=this->_pServer->getClients();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//this->sendJson(jwrite,false);
	_pServer->addToAllertQueue(jwrite);
	_pServer->allertQueue();

	//_pServer->allertPlayers(jwrite);
}
template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::updateAll()
{
	json jwrite;
	jwrite["Type"]="Players";
	jwrite["data"]=this->_pServer->getClients();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//this->sendJson(jwrite,false);
	addtoQueue(jwrite);
	//_pServer->addToAllertQueue(jwrite);

	jwrite.clear();
	jwrite["Type"]="Games";
	jwrite["data"]=this->_pServer->getGames();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//_pServer->allertPlayers(jwrite);
	//this->sendJson(jwrite);
	addtoQueue(jwrite);
	sendQueue(false);
	//_pServer->addToAllertQueue(jwrite);
	//_pServer->allertQueue();
}
template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::updateAllEach()
{
	json jwrite;
	jwrite["Type"]="Players";
	jwrite["data"]=this->_pServer->getClients();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//this->sendJson(jwrite,false);
	//addtoQueue(jwrite);
	_pServer->addToAllertQueue(jwrite);

	jwrite.clear();
	jwrite["Type"]="Games";
	jwrite["data"]=this->_pServer->getGames();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//_pServer->allertPlayers(jwrite);
	//this->sendJson(jwrite);
	//addtoQueue(jwrite);
	//sendQueue(false);
	_pServer->addToAllertQueue(jwrite);
	_pServer->allertQueue();
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::updateGames()
{
	json jwrite;
	jwrite["Type"]="Games";
	jwrite["data"]=this->_pServer->getGames();
	//std::cout<<"Id: "<<_Id<<" "<<jwrite<<std::endl;
	//_pServer->allertPlayers(jwrite);
	_pServer->addToAllertQueue(jwrite);
	_pServer->allertQueue();
	//this->sendJson(jwrite);
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::messageSent( const boost::system::error_code & err, size_t bytes)
{
	json j;
	if (!err)
	{
		//std::cout<<"messageSent - id: "<<_Id<<j<<std::endl;
		if(bytes<response.size())
			sendJson(j,true);
		else
			read();
		return ;
	}
	else
	{
		//std::cout<<"id: "<<this->_Id<<" write error"<<std::endl;
		_socket.close();
		delete this;
	}
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::getStatus()
{
	return _status;
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::getRead()
{
	return _read;
}

template <class TGame, class TPlayer> bool GameServer<TGame,TPlayer>::Client::isReady()
{
	return _ready;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::setReady(bool ready)
{
	_ready=ready;
}

template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::attachPlayer(boost::shared_ptr<TPlayer> player)
{
	_player=player;
	//std::cout<<"Id: "<<getId()<<" player attached"<<std::endl;
}

template <class TGame, class TPlayer> boost::shared_ptr<TPlayer> GameServer<TGame,TPlayer>::Client::getPlayer()
{
	return _player;
}

template <class TGame, class TPlayer> boost::shared_ptr<TGame> GameServer<TGame,TPlayer>::Client::getGame()
{
	return _gra;
}

template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::addtoQueue(json j)
{
	_kolejkaWiadomosci.push(j);
	return _kolejkaWiadomosci.size();
}
template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::sendQueue(bool repeat)
{
	if(!repeat)
	{	if(_kolejkaWiadomosci.size())
		{
			std::stringstream ss;
			boost::system::error_code err;

			_kolejkaWiadomosci.front()>>ss;
			_kolejkaWiadomosci.pop();

			response.clear();
			response = ss.str();
			response = encodeData(&response);

			//send message;
			//std::cout<<"sendQueue id: "<<_Id<<" "<<response<<" left: "<<_kolejkaWiadomosci.size()<<std::endl;
			try
			{
				_socket.async_send(asio::buffer(response.c_str(),response.size()),boost::bind(&Client::sentFromQueue,shared_from_this(),_1,_2));
			}
			catch(std::exception& e)
			{
				//std::cout<<"async_write exception: "<<e.what()<<std::endl;
			}
		}
		else
		{
			read();
		}
	}
	else
	{
		try
		{
			_socket.async_send(asio::buffer(response.c_str(),response.size()),boost::bind(&Client::sentFromQueue,shared_from_this(),_1,_2));
		}
		catch(std::exception& e)
		{
			//std::cout<<"async_write exception: "<<e.what()<<std::endl;
		}
	}
}
template <class TGame, class TPlayer> void GameServer<TGame,TPlayer>::Client::sentFromQueue(const boost::system::error_code & err, size_t bytes)
{
	json j;
	if (!err)
	{
		//std::cout<<"messageSent - id: "<<_Id<<j<<std::endl;
		if(bytes<response.size())
		{
			sendQueue(true);
		}
		else
			if(_kolejkaWiadomosci.size())
			{
				sendQueue(false);
			}
			else
				read();
		return ;
	}
	else
	{
		std::cout<<"id: "<<this->_Id<<" write error"<<std::endl;
		_socket.close();
		delete this;
	}
}
template <class TGame, class TPlayer> int GameServer<TGame,TPlayer>::Client::detachGame()
{
	json j;
	j["Type"]="break";

	//_gra->setState(state::finished);
	//std::cout<<"removing player"<<std::endl;
	if(_gra.use_count()>0)
	{
		_gra->removePlayer(_Id);
		_gra->addToPlayerQueue(j);
	}
	else
	{
		std::cout<<" no game "<<std::endl;
	}
	 //std::cout<<"Player removed"<<std::endl;
	//_gra->sendToPlayers(j);

	//_gra->sendQueueToPlayers();

	if(_gra.use_count()>2)
	{
		//std::cout<<"id: "<<_Id<<" detaching"<<std::endl;
		_gra.reset();
	}
	else
	{
		//std::cout<<"id: "<<_Id<<" detaching and clearing"<<std::endl;
		_gra.reset();
		_pServer->clearGames();
		//std::cout<<"id: "<<_Id<<" game clearing finished"<<std::endl;
	}
	return 0;
}





#endif /* GAMESERVER_H_ */
