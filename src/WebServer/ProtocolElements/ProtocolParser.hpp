/*
 * ProtocolParser.hpp
 *
 *  Created on: 4 mar 2017
 *      Author: Allchemik
 */

#ifndef PROTOCOLPARSER_HPP_
#define PROTOCOLPARSER_HPP_

#include <vector>
#include <map>
#include <string>
#include <iterator>

#include <iostream>

class ProtocolParser{
public:
	ProtocolParser();
	~ProtocolParser();

	/*
	 * parse message into protocol field
	 * input: message to parse
	 * return number of fields;
	 */
	int parseMessage(std::string);

	/*
	 * find protocol field
	 * return values of field as vector of strings;
	 */
	std::vector<std::string> find(std::string);
	/*
	 * prints all fields;
	 */
	void printAll();

private:
	std::map<std::string,std::vector<std::string> > _protocolFields;
	std::vector<std::string>_FieldNames;
};

#endif /* PROTOCOLPARSER_HPP_ */
