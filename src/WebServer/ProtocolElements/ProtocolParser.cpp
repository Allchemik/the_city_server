/*
 * ProtocolParser.cpp
 *
 *  Created on: 4 mar 2017
 *      Author: Allchemik
 */

#include "ProtocolParser.hpp"

using namespace std;

ProtocolParser::ProtocolParser()
{

}


ProtocolParser::~ProtocolParser()
{

}

int ProtocolParser::parseMessage(string message)
{
	string fieldName;
	string singleWord;
	vector<string> fieldData;

	int startWord = 0;
	int endWord = 0;

	for(auto iter = message.begin();iter!=message.end();iter++)
	{
		switch(*iter)
		{
			case ':':
				endWord++;
				fieldName = message.substr(startWord,endWord-startWord-1);
				startWord=endWord;
				//if(iter!=message.end())
				//	startWord++;
				_FieldNames.push_back(fieldName);
				//cout<<".............."<<fieldName<<"."<<endl;
				break;
			case ';':
				endWord++;
				singleWord =message.substr(startWord,endWord-startWord-1);
				startWord=endWord;
				//if(iter!=message.end())
				//	startWord++;
				fieldData.push_back(singleWord);
				break;
			case '\n':
				endWord++;
				singleWord =message.substr(startWord,endWord-startWord-2);
				startWord=endWord;
				//if(iter!=message.end())
				//	startWord++;
				fieldData.push_back(singleWord);
				_protocolFields.insert(std::pair<string,vector<string>>(fieldName,fieldData));
				fieldData.clear();
				fieldName.clear();
				break;
			default:
				endWord++;
		}
	}
	return _protocolFields.size();
}

vector<string> ProtocolParser::find(string field)
{

	return this->_protocolFields[field];
}

void ProtocolParser::printAll()
{
	for(auto iter : _FieldNames)
	{
		cout<<iter<<" : ";
		for(auto iterinner : this->_protocolFields[iter])
		{
			cout<<iterinner<<",";
		}
		cout<<endl;
	}
}
