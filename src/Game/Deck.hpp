/*
 * Deck.hpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#ifndef DECK_HPP_
#define DECK_HPP_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
#include <random>
#include "json.hpp"

using json=nlohmann::json;

enum Symbol {
	Kultura,
	Transport,
	Handel
};

struct Points{
	int baza;
	std::pair<int,int> symbol;//punkty,symbol
	std::pair<int,int> budynek;
	std::pair<int,int> budynekwGrze;
	std::pair<int,int> symbolPrzeciwnik;
};

struct Symbole{
	int kultura=0;
	int handel=0;
	int transport =0;
};

struct Limit{
	std::vector<int> limit;
};

struct Specjal{
	std::pair<int,int> zmniejszKosztZaBudynek;
	std::pair<int,int> zmniejszKosztZaSymbol;
	std::pair<int,std::vector<int>> zmniejszKosztZaBudynki;
	bool ekipa=false;
};

struct Card{
	int _ilosc;
	int _id;
	int _cena;
	Points _karty;
	Points _zwyciestwo;
	Symbole _symbole;
	int _unikat;
	Limit _limit;
	Specjal _specjalne;
	Card(int il,int id,int cena,Points karty,Points zwyciestwo,Symbole symbole,int unikat,Limit limit,Specjal specjalne):_ilosc(il),_id(id),_cena(cena),_karty(karty),_zwyciestwo(zwyciestwo),_symbole(symbole),_unikat(unikat),_limit(limit),_specjalne(specjalne){}
	Card(){};
	std::string ToString()
	{
		std::string text;
		//text.append("ilosc: ");
		//text.append(std::to_string(_il));
		//text.push_back(';');

		text.append("id: ");
		text.append(std::to_string(_id));
		text.push_back(';');

		text.append("cena: ");
		text.append(std::to_string(_cena));
		text.push_back(';');

		text.append("Symbole: ");
		text.append("K: ");
		text.append(std::to_string(_symbole.kultura));
		text.push_back(' ');
		text.append("h: ");
		text.append(std::to_string(_symbole.handel));
		text.push_back(' ');
		text.append("t: ");
		text.append(std::to_string(_symbole.transport));
		text.push_back(' ');
		text.push_back(';');

		text.append("Limit: ");
		for(int i=0;i<_limit.limit.size();++i)
		{
			text.append(std::to_string(_limit.limit.at(i)));
			text.push_back(' ');
		}
		text.push_back(';');

		text.append("unikat: ");
		text.append(std::to_string(_unikat));
		text.push_back(';');

		return text;
	}
};

/*
struct Card{
	int _id;
	int _cena;
	std::string _karty;
	std::string _zwyciestwo;
	std::string _symbole;
	int _unikat;
	std::string _limit;
	std::string _specjalne;
	Card():_id(0),_cena(0),_karty(""),_zwyciestwo(""),_symbole(""),_unikat(0),_limit(""),_specjalne(""){};
	Card(int id, int cena, std::string karty,std::string zwyciestwo, std::string symbole, int unikat, std::string limit , std::string specjalne):
		_id(id),_cena(cena),_karty(karty),_zwyciestwo(zwyciestwo),_symbole(symbole),_unikat(unikat),_limit(limit),_specjalne(specjalne){};
};
*/

class Deck
{
public:
	Deck();

	void loadCards(std::string);
	void shuffle();
	Card* getCard();
	void toTrash(Card*);
	void renewDeck();
	std::string toString();
	std::string toString(int);
	void toJson(json&);
	void toJson(json& ,int);
	void parsePoints(Points* points, std::string str);



private:
	std::map<int,Card>_karty;
	std::vector<Card*> _talia;
	std::vector<Card*> _odrzucone;
};





#endif /* DECK_HPP_ */
