/*
 * Player.hpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <json.hpp>
#include "Deck.hpp"
#include "../WebServer/GameServer.hpp"
#include "Player.hpp"
#include "BuildCityGame.hpp"

class Player;
class BuildCityGame;

//using Client = GameServer<EmpireGame,Player>::Client;

class Player:public boost::enable_shared_from_this<Player>{
public:
	Player();
	Player(boost::shared_ptr<GameServer<BuildCityGame,Player>::Client> client);
	//Player(boost::shared_ptr<GameServer<EmpireGame,Player>::Client> client);
	Player(int id,std::string name);
	std::string getName();
	void setName(std::string);

	int getHandSize();

	int choseCard(int id); // wybiera karte z reki
	int checkLimit(Card* karta); //sprawdz limit karty
	int checkUnique(Card* karta); // sprawdz czy jest unikatem
	int calculateCost(Card* karta); // oblicz koszt
	int findCard(int id); //znajduje karte w miescie


	int buildCard(int id); // buduj karte
	Card* trashCard(int id_Hand); // wyrzuc karte z reki
	int drawCard(Card* karta); // dociagnij karte do reki

	Symbole calculateSymbols(); // oblicz symbole w miescie
	Symbole getSymbols(); // wez symbole w miescie

	void buildArchitect();
	int isArchitect();
	void setEkipa();
	int isEkipa();
	bool isCardEkipa(int);

	int calcualteCardsToDraw();//oblicz karty do dociagu
	int getCardsToDraw(); // pobiera ilosc kart do dociagu

	int updatePoints(); //dodaje pubnkty
	int getPoints(); // pobiera punkty gracza

	int getId();
	void setId(int id);

	bool isTurnExecuted();
	void setTurnExecuted();
	void resetTurnExecuted();

	std::string toStrng();
	json toJson();

	// resign action
	int getResignSize();
	int addToResign(Card* karta);
	Card* TrashFromResign();
	void chooseFromResign(int);
	json resignToJson();
private:
	int _id;
	std::string _name;

	std::vector<Card*> _reka;
	std::vector<Card*> _miasto;
	std::vector<Card*> _resignPool;
	int _punkty=0;
	int _turnExecuted=false;

	boost::shared_ptr<GameServer<BuildCityGame,Player>::Client> _owner;
	int _karty_dociag=0;
	int _punkty_zMiasta=0;
	int _architekt=0;
	int _ekipaBudowlana=0;

	Symbole _symbole;
};



#endif /* PLAYER_HPP_ */
