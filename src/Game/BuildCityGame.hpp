/*
 * EmpireGame.h
 *
 *  Created on: 17 kwi 2017
 *      Author: user
 */
#ifndef EMPIREGAME_H_
#define EMPIREGAME_H_

#include <iostream>
#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include "../WebServer/GameServer.hpp"
//#include "Mapa.hpp"
#include "Deck.hpp"

#include "Player.hpp"

#include "json.hpp"

#define failure 0

#define succes 1

//class Client;
class Player;
class BuildCityGame;

using json =nlohmann::json;

/*enum state{
	newGame,
	waitingPlayers,
	ready,
	started,
	Game_Prepared,//clienci narysowali plansze wysylamy karty
	Send_Bet,//karty otzymane, wyslac licytacje
	Game_Init,//wybrano pierwszego gracza -> rozpoczecie normalnej gry:
	Game_Ongoing,
	finished
};
*/

using Client = GameServer<BuildCityGame,Player>::Client;

class BuildCityGame :boost::noncopyable{
public:
	BuildCityGame(boost::shared_ptr<Client> owner,int id,int players,int mapa,std::string name);
	virtual ~BuildCityGame();
	void addPlayer(boost::shared_ptr<Client> client);
	void removePlayer(boost::shared_ptr<Client> client);
	void removePlayer(int id);
	void setName(std::string nazwa);

	std::string getName();
	int getId();
	void setId(int id);
	int getState();
	void setState(int state);
	void sendPreparationGameInfo();
	json GetPreparationGameInfo();
	void sendToPlayers(json);
	void sendQueueToPlayers();
	void addToPlayerQueue(json);
	bool isGameReady();
	void startGame();

	json getGameInfo();
	json getPlayers();
	void execute(json);
	void executeGame(json);
	void executePreparation(json);

	void InitGame();
	bool isTurnDone();

	json preparePlayersData();
	json prepareRawPlayersData();

	int findCardInGame(int id);
	int findMaxSymbolInGame(int sym,int id);

	void clearGame();

private:
	std::string _name;
	int _Id;
	int _state = 0;

	boost::shared_ptr<Client> _Owner;
	std::map<int,boost::shared_ptr<Client>> _Players;
	std::map<int,bool>_received;

	int _maxPlayers;
	int _maxCards;

	Deck _talia;



};

#endif /* EMPIREGAME_H_ */
