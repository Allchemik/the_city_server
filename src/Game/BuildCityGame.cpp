/*
 * EmpireGame.cpp
 *
 *  Created on: 17 kwi 2017
 *      Author: user
 */

#include "BuildCityGame.hpp"

BuildCityGame::BuildCityGame(boost::shared_ptr<Client> owner,int id,int players,int mapa, std::string name):_Owner(owner),_Id(id),_maxPlayers(players),_name(name)
{
	//_state = state::waitingPlayers;
	//std::cout<<"konstruktor gry "<<std::endl;
	if(_Players.size()<_maxPlayers)
	{
		_Players.insert(std::pair<int,boost::shared_ptr<Client>>(_Owner->getId(),_Owner));
		//std::cout<<"gracz: "<<_Owner->getId()<<" dolaczyl do gry: "<< _Id<<std::endl;
		_Owner->addtoQueue(GetPreparationGameInfo());
	}
	else
		_Owner->read();

	setState(state::waitingPlayers);
	//addPlayer(_Owner);
}

BuildCityGame::~BuildCityGame() {
	//std::cout<<"destruktor gry: "<<_Id<<std::endl;
	// TODO Auto-generated destructor stub
}

void BuildCityGame::addPlayer(boost::shared_ptr<Client> client)
{
	if(_Players.size()<_maxPlayers)
	{
		_Players.insert(std::pair<int,boost::shared_ptr<Client>>(client->getId(),client));
		//std::cout<<"gracz: "<<client->getId()<<" dolaczyl do gry: "<< _Id<<std::endl;
	}
	else
		client->read();
	sendPreparationGameInfo();
}
void BuildCityGame::removePlayer(boost::shared_ptr<Client> client)
{
	//std::cout<<"gracze "<<_Players.size()<<std::endl;
	_Players.erase(client->getId());

}
void BuildCityGame::removePlayer(int id)
{
	//std::cout<<"gracze "<<_Players.size()<<std::endl;
	try
	{
		_Players.at(id);
	}
	catch(std::exception err)
	{
		//std::cout<<"nie znaleziono gracza "<<_Players.size()<<std::endl;
		return;
	}
	_Players.erase(id);
	//std::cout<<"gracze "<<_Players.size()<<std::endl;
}
void BuildCityGame::setName(std::string nazwa)
{
	_name=nazwa;
}
std::string BuildCityGame::getName()
{
	return _name;
}
int BuildCityGame::getId()
{
	return _Id;
}
void BuildCityGame::setId(int id)
{
	_Id=id;
}
int BuildCityGame::getState()
{
	return _state;
}

void BuildCityGame::setState(int stan)
{
	_state = stan;
	//std::cout<<" game state: ";
	switch(_state)
	{
		case state::Game_Init:
			//std::cout<<"Game_Init"<<std::endl;
		break;
		case state::Game_Ongoing:
			//std::cout<<"Game_Ongoing"<<std::endl;
		break;
		case state::Game_Prepared:
			//std::cout<<"Game_Prepared"<<std::endl;
		break;
		case state::Send_Bet:
			//std::cout<<"Send_Bet"<<std::endl;
		break;
		case state::finished:
			//std::cout<<"finished"<<std::endl;
		break;
		case state::newGame:
			//std::cout<<"newGame"<<std::endl;
		break;
		case state::ready:
			//std::cout<<"ready"<<std::endl;
		break;
		case state::started:
			//std::cout<<"started"<<std::endl;
		break;
		case state::waitingPlayers:
			//std::cout<<"waitingPlayers"<<std::endl;
		break;
	}
}
void BuildCityGame::sendPreparationGameInfo()
{
	json jwrite;
	jwrite["Type"]="GameInfo";
	jwrite["data"]=getGameInfo();
	addToPlayerQueue(jwrite);
	sendQueueToPlayers();
	//sendToPlayers(jwrite);
}
json BuildCityGame::GetPreparationGameInfo()
{
	json jwrite;
	jwrite["Type"]="GameInfo";
	jwrite["data"]=getGameInfo();
	return jwrite;
}
void BuildCityGame::sendToPlayers(json j)
{
	for(auto it :this->_Players)
	{
		////std::cout<<" wysyla "<<it.first<<" "<<j<<std::endl;
		it.second->sendJson(j,false);
	}
}

void BuildCityGame::addToPlayerQueue(json j)
{
	for(auto it :_Players)
	{
		if(it.second->getStatus()!=0)
			_Players[it.first]->addtoQueue(j);
	}
}

void BuildCityGame::sendQueueToPlayers()
{
	for(auto it :this->_Players)
	{
		if(_Players[it.first]->getStatus()!=0)
		{
			_Players[it.first]->sendQueue(false);
		}
	}
}

json BuildCityGame::getGameInfo()
{
	json j;
	j["GameId"]=_Id;
	j["owner"]=this->_Owner->getId();
	j["gracze"] = getPlayers();
	return j;
}

json BuildCityGame::getPlayers()
{
	json j;
	for(auto it:_Players)
	{
		json l;
		l["Name"]=it.second->getName();
		l["ready"]=it.second->isReady();
		l["Id"]=it.second->getId();
		j+=l;
	}
	return j;
}

int BuildCityGame::findCardInGame(int id)
{
	int ile=0;
	int temp=0;
	for(auto it:_Players)
	{
		temp=it.second->getPlayer()->findCard(id);
		ile+=(temp!=-1)?temp:0;
	}
	return ile;
}

int BuildCityGame::findMaxSymbolInGame(int sym,int id)
{
	int max=0;
	int ile =0;
	Symbole temp;
	for(auto it:_Players)
	{
		if(it.second->getId()!=id)
		{
			temp=it.second->getPlayer()->getSymbols();
			switch (sym)
			{
				case Symbol::Handel:
					ile = temp.handel;
					break;
				case Symbol::Kultura:
					ile = temp.kultura;
					break;
				case Symbol::Transport:
					ile = temp.transport;
					break;
			}
			if(max<ile)
			{
				max=ile;
			}
		}
	}
	return max;
}


void BuildCityGame::execute(json jin)
{
	switch(_state)
	{
		case state::newGame:
			//std::cout<<"newGame hehe"<<std::endl;
			setState(state::waitingPlayers);
		case state::waitingPlayers:
			executePreparation(jin);
			break;
		case state::ready:
			if(jin["Type"]=="StartGame")
			{
//------> init game
				json j,k;
				//std::cout<<" uruchomienie gry"<<std::endl;
				//przygotuj talie
				_talia.loadCards("karty.txt");
				_talia.shuffle();

				//przygotuj graczy
				for(auto it:_Players)
				{
					//boost::shared_ptr<Client> ptt(_Players.at(it.second->getId()));
					boost::shared_ptr<Player> player(new Player(_Players.at(it.second->getId())));
					_Players[it.second->getId()]->attachPlayer(player);
				}
				//dodaj po 7 kart

				for(auto it:_Players)
				{
					for(int i=0;i<7;++i)
					{
						_Players[it.second->getId()]->getPlayer()->drawCard(_talia.getCard());
					}
					k+=_Players[it.second->getId()]->getPlayer()->toJson();
					//std::cout<<_Players[it.second->getId()]->getPlayer()->toStrng()<<std::endl;
				}

				/*
				 * zapytaj o wyrzucenie 2
				 */
				j["Type"] = "startNewGame";
				j["data"] = k;
				addToPlayerQueue(j);
				sendQueueToPlayers();
				setState(state::started);
			}
			else
			{
				for(auto it :this->_Players)
				{
					it.second->read();
				}
			}
			break;
		case state::started:
			if(jin["Type"]=="GameStarted")
			{
				/*
				 *  otrzymuje info o usunietych kartach od graczy
				 * 	wysyla kiedy otrzyma wszystkie wysyla nowa ture
				 */
//->>>>>>> usunac karty !

				bool executed=true;
				int id = jin["sId"].get<int>();// id gracza
				if(!_Players[id]->getPlayer()->isTurnExecuted())
				{
					for(int i=0;i<jin["data"].size();++i)
					{
						int index = jin["data"][i];
						_talia.toTrash(_Players[id]->getPlayer()->trashCard(index-i));
						//std::cout<<jin["data"][i]<<std::endl;
					}
					_Players[id]->getPlayer()->setTurnExecuted();
				}
				for(auto it:_Players)
				{
					if(!_Players[it.second->getId()]->getPlayer()->isTurnExecuted())
					{
						_Players[it.second->getId()]->read();
						executed = false;
					}
					else
					{
						_Players[id]->read();
					}
				}

				if(executed)
				{
					json j,k;
					for(auto it:_Players)
					{
						k+=_Players[it.second->getId()]->getPlayer()->toJson();
						//std::cout<<_Players[it.second->getId()]->getPlayer()->toStrng()<<std::endl;
						_Players[it.second->getId()]->getPlayer()->resetTurnExecuted();
					}

					j["Type"] = "newTurn";
					j["data"] = k;
					addToPlayerQueue(j);

					sendQueueToPlayers();
					setState(state::Game_Ongoing);
				}
			}
			break;
		case state::Game_Ongoing:
			executeGame(jin);
			break;
		case state::finished:
			// wroc gracza do lobby
			// usun gre;
			break;
	}
}

void BuildCityGame::executeGame(json jin)
{
	json j;

	int id = jin["sId"].get<int>();// id gracza
	int response=-1;

	if(jin["Type"]=="Build")
	{
		//std::cout<<id<<" buduje"<<std::endl;
		if(!_Players[id]->getPlayer()->isTurnExecuted())
		{
			if(_Players[id]->getPlayer()->isEkipa())
			{
				int CardCost =0;
				bool CardsOk=false;
				for(int i=0;i<jin["data"].size();++i)
				{
					int index = jin["data"][i];
					response=_Players[id]->getPlayer()->choseCard(index);
					if(response!=-1)
					{
						CardsOk=true;
						//_Players[id]->getPlayer()->buildCard(index-i);
						////std::cout<<"zbudowano "<<jin["data"][i]<<std::endl;
						CardCost+=response;
					}
					else
					{
						CardsOk=false;
						//std::cout<<"error " <<jin["data"][i]<<std::endl;
						break;
					}
				}
				if((CardCost<=(_Players[id]->getPlayer()->getHandSize()-jin["data"].size()))&&CardsOk)
				{
					for(int i=0;i<jin["data"].size();++i)
					{
						int index = jin["data"][i];
						_Players[id]->getPlayer()->buildCard(index-i);
						//std::cout<<"zbudowano karte"<<jin["data"][i]<<std::endl;
					}
					json k,l;
					j["Type"]="Pay";
					j["data"]=prepareRawPlayersData();
					j["cost"]=CardCost;
					_Players[id]->addtoQueue(j);
				}
				else
				{
					//std::cout<<"error " <<CardCost<<" "<<_Players[id]->getPlayer()->getHandSize()-jin["data"].size()<<std::endl;
				}
			}
			else
			{
				//std::cout<<"gdzies tu moze byc ekipa "<< jin["data"].size()<<std::endl;
				int CardCost =0;
				bool build2=false;
				bool findEkipa=false;

				if((jin["data"].size()>1)&&(_Players[id]->getPlayer()->isEkipa()!=1))
				{
					//std::cout<<"wiecej niz 2 i niema jeszcze ekipy"<<std::endl;
					for(int i=0;i<jin["data"].size();++i)
					{
						int index = jin["data"][i];
						//std::cout<<"czy karta  jest ekipa "<<jin["data"][i]<<std::endl;
						if(_Players[id]->getPlayer()->isCardEkipa(index))
						{
							response=_Players[id]->getPlayer()->choseCard(index);
							if(response!=-1)
							{
								//std::cout<<"znaleziono architekta i koszt ok"<<std::endl;
								CardCost+=response;
								findEkipa=true;
							}
						}
						else
						{
							response=_Players[id]->getPlayer()->choseCard(index);
							if(response!=-1)
							{
								//std::cout<<"to nie architekt"<<std::endl;
								CardCost+=response;
							}
						}
					}
					if((findEkipa==true) && (CardCost<=(_Players[id]->getPlayer()->getHandSize()-jin["data"].size())))
					{
						//std::cout<<"znaleziono ekipe i buduje karty"<<std::endl;
						for(int i=0;i<jin["data"].size();++i)
						{
							int index = jin["data"][i];
							_Players[id]->getPlayer()->buildCard(index-i);
							//std::cout<<"zbudowano ! "<<jin["data"][i]<<std::endl;
						}
						//std::cout<<"znaleziono ekipe i przygotowuje dane do zaplaty"<<std::endl;

						j["Type"]="Pay";
						j["data"]=prepareRawPlayersData();
						j["cost"]=CardCost;
						_Players[id]->addtoQueue(j);
					}
				}
				else
				{
					for(int i=0;i<jin["data"].size();++i)
					{
						int index = jin["data"][i];
						//std::cout<<"index "<<index <<std::endl;
						response=_Players[id]->getPlayer()->choseCard(index-i);
						if(response!=-1)
						{

							_Players[id]->getPlayer()->buildCard(index-i);
							//std::cout<<"zbudowano "<<jin["data"][i]<<std::endl;
							j["Type"]="Pay";
							j["data"]=prepareRawPlayersData();
							j["cost"]=response;
							_Players[id]->addtoQueue(j);
							//addToPlayerQueue(j);
						}
						else
						{
							//std::cout<<"zle zinterpretowano tablice"<<std::endl;
						}
					}
				}
			}
		}
	}
	else if(jin["Type"]=="Pay")
	{
		if(!_Players[id]->getPlayer()->isTurnExecuted())
		{
			for(int i=0;i<jin["data"].size();++i)
			{
				int index = jin["data"][i];
				_talia.toTrash(_Players[id]->getPlayer()->trashCard(index-i));
				//std::cout<<jin["data"][i]<<std::endl;
			}
			_Players[id]->getPlayer()->setTurnExecuted();

			j["Type"]="PayOk";
			j["data"]=1;
			_Players[id]->addtoQueue(j);
		}
	}
	else if(jin["Type"]=="Architect")
	{
		if(!_Players[id]->getPlayer()->isTurnExecuted())
		{
			_Players[id]->getPlayer()->setTurnExecuted();
			_Players[id]->getPlayer()->buildArchitect();
			_Players[id]->read();

			j["Type"]="PayOk";
			j["data"]=1;
			_Players[id]->addtoQueue(j);
		}
	}
	else if(jin["Type"]=="Resign")
	{
		if(!_Players[id]->getPlayer()->isTurnExecuted())
		{
			for(int i=0;i<5;++i)
			{
				_Players[id]->getPlayer()->addToResign(_talia.getCard());
			}
			j["Type"]="Resign";
			j["data"]=_Players[id]->getPlayer()->resignToJson();
			_Players[id]->addtoQueue(j);
		}
	}
	else if(jin["Type"]=="Choice")
	{
		if(!_Players[id]->getPlayer()->isTurnExecuted())
		{
			_Players[id]->getPlayer()->setTurnExecuted();
			int index = jin["data"];
			_Players[id]->getPlayer()->chooseFromResign(index);
			for(int i=0;i<4;++i)
			{
				_talia.toTrash(_Players[id]->getPlayer()->TrashFromResign());
			}
			j["Type"]="PayOk";
			j["data"]=1;
			_Players[id]->addtoQueue(j);
		}
	}
	else
	{

	}

	if(isTurnDone())
	{
		bool endgame=false;
		for(auto it:_Players)
		{
			_Players[it.second->getId()]->getPlayer()->calculateSymbols();
		}
		for(auto it:_Players)
		{
			int draw = _Players[it.second->getId()]->getPlayer()->calcualteCardsToDraw();
			for(int i=0;i<draw;++i)
			{
				if(_Players[it.second->getId()]->getPlayer()->drawCard(_talia.getCard())>=12)
				{
					break;
				}
			}
			if(_Players[it.second->getId()]->getPlayer()->updatePoints()>=50)
			{
				endgame=true;
			}
			_Players[it.second->getId()]->getPlayer()->resetTurnExecuted();
		}
		/*sprawdz koniec gry*/
		if(endgame)
		{
			j["Type"]="end";
			j["data"]=prepareRawPlayersData();
		}
		else
		{
			j=preparePlayersData();
		}
		addToPlayerQueue(j);
	}
	else
	{
		//addToPlayerQueue(j);
	}
	sendQueueToPlayers();
}

void BuildCityGame::executePreparation(json jin)
{
	////std::cout<<"gra Id: "<<_Id<<" "<<jin<<std::endl;
	if(jin["Type"]=="Ready")
	{
		int id = jin["sId"].get<int>();
		_Players[id]->setReady(!_Players[id]->getRead());
		isGameReady();
		sendPreparationGameInfo();
	}
	else
	{
		for(auto it :this->_Players)
		{
			it.second->read();
		}
	}
}

void BuildCityGame::startGame()
{
	json j;
	setState(state::started);
	j["Type"]="startNewGame";
	j["data"]["gracze"]=_maxPlayers;
	sendToPlayers(j);
}

bool BuildCityGame::isGameReady()
{
	if(_Players.size()<_maxPlayers)
		return false;
	for(auto it:_Players)
	{
		if(!it.second->isReady())
		{
			return false;
		}
	}
	setState(state::ready);
	return true;
}

void BuildCityGame::clearGame()
{
	//std::map<int,boost::shared_ptr<Client>>::iterator it;
	for(auto it:_Players)
	{
		_Players[it.second->getId()]->detachGame();
		setState(state::finished);
	}
}

bool BuildCityGame::isTurnDone()
{
	bool executed = true;
	for(auto it:_Players)
	{
		if(!_Players[it.second->getId()]->getPlayer()->isTurnExecuted())
		{
			_Players[it.second->getId()]->read();
			executed = false;
		}
		else
		{
			_Players[it.second->getId()]->read();
		}
	}
	return executed;
}

json BuildCityGame::preparePlayersData()
{
	json j,k;
	for(auto it:_Players)
	{
		k+=_Players[it.second->getId()]->getPlayer()->toJson();
		////std::cout<<_Players[it.second->getId()]->getPlayer()->toStrng()<<std::endl;
	}
	j["Type"] = "newTurn";
	j["data"] = k;

	return j;
}
json BuildCityGame::prepareRawPlayersData()
{
	json k;
	for(auto it:_Players)
	{
		k+=_Players[it.second->getId()]->getPlayer()->toJson();
		////std::cout<<_Players[it.second->getId()]->getPlayer()->toStrng()<<std::endl;
	}

	return k;
}



