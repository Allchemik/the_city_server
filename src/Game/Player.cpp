/*
 * Player.cpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#include "Player.hpp"

Player::Player():_id(0),_name("anonymous")
{

}
Player::Player(boost::shared_ptr<GameServer<BuildCityGame,Player>::Client> client):_id(client->getId()),_name(client->getName()),_owner(client)
{
	std::cout<<"Player created "<<client.use_count()<<std::endl;
	//wywoluje destruktor....
	//_owner->attachPlayer(shared_from_this());
}

Player::Player(int id,std::string name):_id(id),_name(name)
{

}

std::string Player::getName()
{
	return _name;
}

void Player::setName(std::string name)
{
	_name = name;
}

int Player::getHandSize()
{
	return this->_reka.size();
}

int Player::getId()
{
	return _id;
}

void Player::setId(int id)
{
	this->_id=id;
}
bool Player::isTurnExecuted()
{
	return _turnExecuted;
}
void Player::setTurnExecuted()
{
	_turnExecuted=true;
}
void Player::resetTurnExecuted()
{
	_turnExecuted=false;
}
void Player::buildArchitect()
{
	_architekt =true;
}
void Player::setEkipa()
{
	_ekipaBudowlana=true;
}

bool Player::isCardEkipa(int id)
{
	Card* karta;
	karta = this->_reka.at(id);

	std::cout<<"czy to ekipa "<<karta->ToString()<<std::endl;

	if(karta->_id==7)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Player::choseCard(int id)
{
	Card* karta;
	int response;
	try
	{
	karta = this->_reka.at(id);
	}
	catch(std::exception e)
	{
		std::cout<<"!!!!!!!wyjątek karty z reki "<<e.what()<<std::endl;
	}
	std::cout<<karta->ToString()<<std::endl;
	//std::cout<<"limit "<<std::endl;
	response=checkLimit(karta);

	std::cout<<"limit "<<response<<std::endl;

	if(response==-1)
	{
		std::cout<<"nie spelniasz warunkow budowy "<<karta->_limit.limit.size()<<karta->_limit.limit[0]<<std::endl;
		return -1;
		//error;
	}
	response=checkUnique(karta);

	std::cout<<"unikat "<<response<<std::endl;

	if(response==-1)
	{
		std::cout<<"karta unikalna"<<std::endl;
		return -1;
			//error;
	}
	response = calculateCost(karta);

	std::cout<<"koszt "<<response<<std::endl;

	if(response>(this->_reka.size()-1))
	{
		std::cout<<response<<" nie stac cie na karte "<<_reka.size()<<std::endl;
		return -1;
		//error
	}

	return response;
}

int Player::checkLimit(Card* karta)
{
	if(karta->_limit.limit.size()<1)
	{
		return 0;
	}
	for(int i=0;i<karta->_limit.limit.size();++i)
	{
		if(findCard(karta->_limit.limit.at(i))!=-1)
		{
			return 0;
		}
		else
		{
			std::cout<<"potrzeba :"<<karta->_limit.limit.at(i)<<std::endl;
		}
	}
	return -1;
}
int Player::checkUnique(Card* karta)
{
	if(karta->_unikat)
	{
		if(findCard(karta->_id)!=-1)
		{
			return -1;
		}
	}
	return 0;
}

int Player::calculateCost(Card* karta)
{
	int id = karta->_id;
	int modyfikator =0;

	for(int i=0;i<_miasto.size();++i)
	{
		std::cout<<"miasto "<<i<<std::endl;

		if(_miasto.at(i)->_specjalne.zmniejszKosztZaBudynek.second==id)
		{
			modyfikator+=_miasto.at(i)->_specjalne.zmniejszKosztZaBudynek.first;
		}
		switch(_miasto.at(i)->_specjalne.zmniejszKosztZaSymbol.second)
		{
			case Kultura:
				if(_symbole.kultura>0)
					modyfikator+=_miasto.at(i)->_specjalne.zmniejszKosztZaSymbol.first;
				break;
			case Transport:
				if(_symbole.transport>0)
					modyfikator+=_miasto.at(i)->_specjalne.zmniejszKosztZaSymbol.first;
				break;
			case Handel:
				if(_symbole.handel>0)
					modyfikator+=_miasto.at(i)->_specjalne.zmniejszKosztZaSymbol.first;
				break;
		}

		for(int i=0;i<_miasto.at(i)->_specjalne.zmniejszKosztZaBudynki.second.size();++i)
		{
			if(_miasto.at(i)->_specjalne.zmniejszKosztZaBudynki.second.at(i)==id)
			{
				modyfikator+=_miasto.at(i)->_specjalne.zmniejszKosztZaBudynki.first;
			}
		}
	}
	std::cout<<"mod: "<<modyfikator<<std::endl;

	return karta->_cena-modyfikator;
}

int Player::findCard(int id)
{
	int ile=0;
	for(int i=0;i<_miasto.size();++i)
	{
		if(_miasto.at(i)->_id==id)
		{
			ile++;
		}
	}
	return (ile==0)?-1:ile;
}

int Player::buildCard(int id)
{
	Card* karta = _reka.at(id);
	_miasto.push_back(karta);
	_reka.erase(_reka.begin()+id);

	if(karta->_specjalne.ekipa)
	{
		this->_ekipaBudowlana=1;
	}
	return _reka.size();
}
Card* Player::trashCard(int id_Hand)
{
	Card* karta = _reka.at(id_Hand);
	_reka.erase(_reka.begin()+id_Hand);
	return karta;
}
int Player::drawCard(Card* karta)
{
	_reka.push_back(karta);
	return _reka.size();
}

Symbole Player::calculateSymbols()
{
	Symbole sym;
	sym.handel=0;
	sym.kultura=0;
	sym.transport=0;
	for(int i=0;i<_miasto.size();++i)
	{
		sym.handel+=_miasto.at(i)->_symbole.handel;
		sym.kultura+=_miasto.at(i)->_symbole.kultura;
		sym.transport+=_miasto.at(i)->_symbole.transport;
	}
	_symbole=sym;
	return _symbole;
}

Symbole Player::getSymbols()
{
	return _symbole;
}

int Player::isArchitect()
{
	return _architekt;
}
int Player::isEkipa()
{
	return _ekipaBudowlana;
}

int Player::updatePoints()
{
	int pkt=0;
	int ville =0;
	for(int i=0;i<_miasto.size();++i)
	{
		if(_miasto.at(i)->_id==21)
		{
			ville++;
		}

		pkt+=_miasto.at(i)->_zwyciestwo.baza;
		pkt+=findCard(_miasto.at(i)->_zwyciestwo.budynek.second)!=-1?_miasto.at(i)->_zwyciestwo.budynek.first:0;

		pkt+=_owner->getGame()->findCardInGame(_miasto.at(i)->_zwyciestwo.budynekwGrze.second)*_miasto.at(i)->_zwyciestwo.budynekwGrze.first;

		switch(_miasto.at(i)->_zwyciestwo.symbol.second)
		{
			case Kultura:
			pkt+=_miasto.at(i)->_zwyciestwo.symbol.first*_symbole.kultura;
			break;

			case Handel:
			pkt+=_miasto.at(i)->_zwyciestwo.symbol.first*_symbole.handel;
			break;

			case Transport:
			pkt+=_miasto.at(i)->_zwyciestwo.symbol.first*_symbole.transport;
			break;
		}

		pkt+=_owner->getGame()->findMaxSymbolInGame(_miasto.at(i)->_zwyciestwo.symbolPrzeciwnik.second,this->_id)*_miasto.at(i)->_zwyciestwo.symbolPrzeciwnik.first;

		/*switch(_miasto.at(i)->_zwyciestwo.symbolPrzeciwnik.second)
		{
			case Kultura:
				pkt+=_owner->getGame()->findMaxSymbolInGame(_miasto.at(i)->_zwyciestwo.symbolPrzeciwnik.second,this->_id)*_miasto.at(i)->_zwyciestwo.symbolPrzeciwnik.first;
				break;
			case Handel:
				break;
			case Transport:
				break;
		}*/
	}
	pkt+=ville*(2*(ville-1));
	_punkty_zMiasta=pkt;
	_punkty+=_punkty_zMiasta;
	return _punkty;
}

int Player::calcualteCardsToDraw()
{
	int kar=0;
	for(int i=0;i<_miasto.size();++i)
	{
		std::cout<<" karta: "<<_miasto.at(i)->ToString()<<std::endl;
		std::cout<<"id: "<<_miasto.at(i)->_id<<" baz: "<<_miasto.at(i)->_karty.baza<<
				" budynek "<<_miasto.at(i)->_karty.budynek.second<<
				" bud w grze "<<_miasto.at(i)->_karty.budynekwGrze.second;

		kar+=_miasto.at(i)->_karty.baza;

		kar+=findCard(_miasto.at(i)->_karty.budynek.second)!=-1?_miasto.at(i)->_karty.budynek.first:0;

		//kar+=findCard(_miasto.at(i)->_karty.budynekwGrze.second)!=-1?_miasto.at(i)->_karty.budynekwGrze.first:0;// +budynki przeciwników

		kar+=_owner->getGame()->findCardInGame(_miasto.at(i)->_karty.budynekwGrze.second)*_miasto.at(i)->_karty.budynekwGrze.first;


		std::cout<<"symbol: "<<_miasto.at(i)->_karty.symbol.second;
		switch(_miasto.at(i)->_karty.symbol.second)
		{
			case Kultura:
				std::cout<<"kultura: "<<_symbole.kultura<<" "<<_miasto.at(i)->_karty.symbol.first*_symbole.kultura<<std::endl;
				kar+=_miasto.at(i)->_karty.symbol.first*_symbole.kultura;
			break;

			case Handel:
				std::cout<<"handel: "<<_symbole.handel<<" "<<_miasto.at(i)->_karty.symbol.first*_symbole.handel<<std::endl;
				kar+=_miasto.at(i)->_karty.symbol.first*_symbole.handel;
			break;

			case Transport:
				std::cout<<"transport: "<<_symbole.transport<<" "<<_miasto.at(i)->_karty.symbol.first*_symbole.transport<<std::endl;
				kar+=_miasto.at(i)->_karty.symbol.first*_symbole.transport;
			break;
		}

		kar+=_owner->getGame()->findMaxSymbolInGame(_miasto.at(i)->_karty.symbolPrzeciwnik.second,this->_id)*_miasto.at(i)->_karty.symbolPrzeciwnik.first;

	/*	switch(_miasto.at(i)->_karty.symbolPrzeciwnik.second)
		{
			case Kultura:
				break;
			case Handel:
				break;
			case Transport:
				break;
		}*/
	}
	if(isArchitect()>0)
	{
		kar+=1;
	}
	_karty_dociag=kar;
	return kar;
}

int Player::getCardsToDraw()
{
	return _karty_dociag;
}

int Player::getPoints()
{
	return _punkty;
}

std::string Player::toStrng()
{
	std::string stringifiedPlayer;

	stringifiedPlayer.append(std::to_string(this->_id));
	stringifiedPlayer.push_back(';');
	stringifiedPlayer.append(this->_name);
	stringifiedPlayer.push_back(';');

	for(auto it :this->_reka)
	{
		stringifiedPlayer.append(std::to_string(it->_id));
		stringifiedPlayer.push_back(',');
	}
	stringifiedPlayer=stringifiedPlayer.substr(0,stringifiedPlayer.size()-1);
	stringifiedPlayer.push_back(';');

	for(auto it :this->_miasto)
	{
		stringifiedPlayer.append(std::to_string(it->_id));
		stringifiedPlayer.push_back(',');
	}
	stringifiedPlayer=stringifiedPlayer.substr(0,stringifiedPlayer.size()-1);
	stringifiedPlayer.push_back(';');

	stringifiedPlayer.append(std::to_string(_karty_dociag));
	stringifiedPlayer.push_back(';');

	stringifiedPlayer.append(std::to_string(_punkty_zMiasta));
	stringifiedPlayer.push_back(';');

	stringifiedPlayer.append(std::to_string(_punkty));
	stringifiedPlayer.push_back(';');

	return stringifiedPlayer;
}

json Player::toJson()
{
	json j,k;

	j["id"]=this->_id;
	j["name"]=this->_name;
	j["dociag"] = this->_karty_dociag;
	j["punkty_miasto"]= this->_punkty_zMiasta;
	j["punkty"]=this->_punkty;
	j["architect"]=this->isArchitect();
	j["ekipa"]=this->isEkipa();

	for (auto it:_reka)
	{
		k.clear();
		k["id"]=it->_id;
		j["karty"]+=k;
	}

	for (auto it:_miasto)
	{
		k.clear();
		k["id"]=it->_id;
		j["miasto"]+=k;
	}
	return j;
}

int Player::getResignSize()
{
	return _resignPool.size();
}

int Player::addToResign(Card* karta)
{
	_resignPool.push_back(karta);
	return _resignPool.size();
}

Card* Player::TrashFromResign()
{
	Card* karta = _resignPool.at(_resignPool.size()-1);
	_resignPool.pop_back();
	//_resignPool.erase(_resignPool.begin()+_resignPool.size()-1);
	return karta;
}

void Player::chooseFromResign(int id)
{
	Card* karta = _resignPool.at(id);
	_reka.push_back(karta);
	_resignPool.erase(_resignPool.begin()+id);
}

json Player::resignToJson()
{
	json j,k;
	for(int i=0;i<_resignPool.size();++i)
	{
		k.clear();
		k["id"]=_resignPool.at(i)->_id;
		j+=k;
	}
	return j;
}
