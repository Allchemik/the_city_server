/*
 * Deck.cpp
 *
 *  Created on: 13 mar 2017
 *      Author: Allchemik
 */

#include "Deck.hpp"

Deck::Deck(){

}

void Deck::loadCards(std::string path)
{
	std::fstream fs;
	std::string kartaTxt,karty,zwyciestwo,symbole,limit,specjalne;
	int id,cena,unikat,ilosc,temp;

	Points* _karty,*_zwyciestwo;
	Symbole* _symbole;
	Limit*_limit;
	Specjal* _specjalne;

	std::string element;
	int  start,end;

	fs.open(path.c_str(), std::fstream::in);
	if(fs.is_open())
	{
		while(!fs.eof())
		{
			start =0;
			fs>>kartaTxt;
			//ilosc
			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>ilosc;
			start = end+1;
			//id
			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>id;
			start = end+1;
			//cena
			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>cena;
			start = end+1;
			//karty
			end = kartaTxt.find(';',start);
			karty = kartaTxt.substr(start,end-start);
			_karty = new Points();
			parsePoints(_karty,karty);
			start = end+1;
			//zwyciestwo
			end = kartaTxt.find(';',start);
			zwyciestwo = kartaTxt.substr(start,end-start);
			_zwyciestwo=new Points();
			parsePoints(_zwyciestwo,zwyciestwo);
			start = end+1;
			//symbole;
			end = kartaTxt.find(';',start);
			symbole = kartaTxt.substr(start,end-start);
			_symbole=new Symbole();
			for(int i=0;i<symbole.size();++i)
			{
				switch(symbole.at(i))
				{
					case 'K':
						_symbole->kultura+=1;
						break;
					case'H':
						_symbole->handel+=1;
						break;
					case 'T':
						_symbole->transport+=1;
						break;
				}
			}
			start = end+1;
			//limit
			end = kartaTxt.find(';',start);
			limit = kartaTxt.substr(start,end-start);
			_limit=new Limit();
			std::string temp;
			for(int i=0;i<limit.size();++i)
			{
				if(limit[i]=='/')
				{

						int nr=-1;
						std::istringstream(temp)>>nr;
						_limit->limit.push_back(nr);
						temp.clear();
				}
				else
				{
					temp+=limit[i];
				}
			}
			if(limit.size()>0)
			{
				int nr;
				std::istringstream(temp)>>nr;
				_limit->limit.push_back(nr);
			}
			start = end+1;
			//unikat
			end = kartaTxt.find(';',start);
			element=kartaTxt.substr(start,end-start);
			std::istringstream(element)>>unikat;
			start = end+1;
			//specjalne
			end = kartaTxt.find(';',start);
			specjalne = kartaTxt.substr(start,end-start);
			start = end+1;
			_specjalne = new Specjal();
			for(int i=0;i<specjalne.size();++i)
			{
				switch(specjalne.at(i))
				{
					case 'K':
					{
						std::string temp1,temp2;
						for(i;i<specjalne.size();++i)
						{
							switch(specjalne.at(i))
							{
								case '/':

								break;
								default:
									if(temp1.size()==0)
										temp1+=specjalne.at(i);
									else
										temp2 += specjalne.at(i);
								break;
							}
						}
						{
						int pkt,symb;
						std::istringstream(temp1)>>pkt;
						std::istringstream(temp2)>>symb;
						_specjalne->zmniejszKosztZaSymbol=std::pair<int,int>(pkt,symb);
						}
					}
						break;
					case 'A':
						_specjalne->ekipa = true;
						break;
					case 'C':
						std::string temp1,temp2;
						int pkt,nr;
						std::vector<int> wektor;
						for(i;i<specjalne.size();++i)
						{
							switch(specjalne.at(i))
							{
								case '^':
									std::istringstream(temp1)>>pkt;
								break;
								case '/':
									std::istringstream(temp2)>>nr;
									wektor.push_back(nr);
									temp2.clear();
								break;
								default:
									if(temp1.size()==0)
										temp1+=specjalne.at(i);
									else
										temp2 += specjalne.at(i);
								break;
							}
						}
						if(temp2.size()>0)
						{
							std::istringstream(temp2)>>nr;
							wektor.push_back(nr);
							_specjalne->zmniejszKosztZaBudynki= std::pair<int,std::vector<int>>(pkt,wektor);
						}
						break;
				}
			}
			//kartaTemp = new Card(id,symbol,ilosc,akcja);

			this->_karty.insert(std::pair<int,Card>(id,Card(ilosc,id,cena,*_karty,*_zwyciestwo,*_symbole,unikat,*_limit,*_specjalne)));
		}
	}
	else
	{
		std::cout<<" unable to open file";
	}
	std::cout<<"kart "<<this->_karty.size()<<std::endl;
	std::cout<<"talia"<<_talia.size()<<std::endl;
}
void Deck::shuffle()
{
	//load new deck;
	_talia.clear();
	std::random_device rd;
	auto engine = std::default_random_engine(rd());
	for(int i=1;i<_karty.size();++i)
	{
		for(int j=0;j<_karty[i]._ilosc;++j)
		{
			this->_talia.push_back(&_karty[i]);
		}
	}
	std::shuffle(std::begin(_talia), std::end(_talia), engine);
	//shuffle
}

Card* Deck::getCard()
{
	if(_talia.size()<1)
		renewDeck();
	Card* karta = _talia[_talia.size()-1];
	_talia.pop_back();
	return karta;
}
void Deck::toTrash(Card* karta)
{
	_odrzucone.push_back(karta);
}

void Deck::renewDeck()
{
	std::random_device rd;
	auto engine = std::default_random_engine(rd());
	while(_odrzucone.size()>0)
	{
		Card* karta = _odrzucone[_odrzucone.size()-1];
		_odrzucone.pop_back();
		_talia.push_back(karta);
	}
	std::shuffle(std::begin(_talia), std::end(_talia), engine);
}


std::string Deck::toString(int id)
{
	std::string strinigifiedCard;

	strinigifiedCard.append(std::to_string(_karty[id]._ilosc));
	strinigifiedCard.push_back(';');
	strinigifiedCard.append(std::to_string(_karty[id]._id));
	strinigifiedCard.push_back(';');
	strinigifiedCard.append(std::to_string(_karty[id]._cena));
	strinigifiedCard.push_back(';');

	if(_karty[id]._karty.baza>-1)
	{
		strinigifiedCard.append(std::to_string(_karty[id]._karty.baza));
	}
	if(_karty[id]._karty.budynek.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.budynek.first));
		strinigifiedCard.push_back('*');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.budynek.second));
	}
	if(_karty[id]._karty.budynekwGrze.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.budynekwGrze.first));
		strinigifiedCard.push_back('^');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.budynekwGrze.second));
	}
	if(_karty[id]._karty.symbol.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.symbol.first));
		strinigifiedCard.push_back('/');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.symbol.second));
	}
	if(_karty[id]._karty.symbolPrzeciwnik.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.symbol.first));
		strinigifiedCard.push_back('@');
		strinigifiedCard.append(std::to_string(_karty[id]._karty.symbol.second));
	}
	strinigifiedCard.push_back(';');
	if(_karty[id]._zwyciestwo.baza>-1)
	{
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.baza));
	}
	if(_karty[id]._zwyciestwo.budynek.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.budynek.first));
		strinigifiedCard.push_back('*');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.budynek.second));
	}
	if(_karty[id]._zwyciestwo.budynekwGrze.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.budynekwGrze.first));
		strinigifiedCard.push_back('^');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.budynekwGrze.second));
	}
	if(_karty[id]._zwyciestwo.symbol.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.symbol.first));
		strinigifiedCard.push_back('/');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.symbol.second));
	}
	if(_karty[id]._zwyciestwo.symbolPrzeciwnik.first!=-1)
	{
		strinigifiedCard.push_back('+');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.symbol.first));
		strinigifiedCard.push_back('@');
		strinigifiedCard.append(std::to_string(_karty[id]._zwyciestwo.symbol.second));
	}
	strinigifiedCard.push_back(';');
	for(int i=0;i< _karty[id]._symbole.handel;++i)
	{
		strinigifiedCard.push_back('H');
	}
	for(int i=0;i< _karty[id]._symbole.kultura;++i)
	{
		strinigifiedCard.push_back('K');
	}

	for(int i=0;i< _karty[id]._symbole.transport;++i)
	{
		strinigifiedCard.push_back('T');
	}
	strinigifiedCard.push_back(';');
	if(_karty[id]._limit.limit.size()>0)
	{
		for(int i=0;i<_karty[id]._limit.limit.size()-1;++i)
		{
			strinigifiedCard+=std::to_string(_karty[id]._limit.limit.at(i));
			strinigifiedCard.push_back('/');
		}
		strinigifiedCard+=std::to_string(_karty[id]._limit.limit.at(_karty[id]._limit.limit.size()-1));
		strinigifiedCard.push_back(';');
	}
	strinigifiedCard.push_back(';');

	if(_karty[id]._specjalne.zmniejszKosztZaBudynek.first>=0)
	{
		strinigifiedCard.push_back('C');
		_karty[id]._specjalne.zmniejszKosztZaBudynek.first;
		strinigifiedCard.push_back('^');
		_karty[id]._specjalne.zmniejszKosztZaBudynek.second;
	}

	if(_karty[id]._specjalne.zmniejszKosztZaBudynki.second.size()>0)
	{
		strinigifiedCard.push_back('C');
		_karty[id]._specjalne.zmniejszKosztZaBudynki.first;
		strinigifiedCard.push_back('^');
		for(int j=0;j<_karty[id]._specjalne.zmniejszKosztZaBudynki.second.size()-1;++j)
		{
			strinigifiedCard+=std::to_string(_karty[id]._specjalne.zmniejszKosztZaBudynki.second.at(j));
			strinigifiedCard.push_back('/');
		}
		strinigifiedCard+=std::to_string(_karty[id]._specjalne.zmniejszKosztZaBudynki.second.at(_karty[id]._specjalne.zmniejszKosztZaBudynki.second.size()-1));
	}
	if(_karty[id]._specjalne.zmniejszKosztZaSymbol.first>=0)
	{
		strinigifiedCard.push_back('K');
		strinigifiedCard+=std::to_string(_karty[id]._specjalne.zmniejszKosztZaSymbol.first);
		strinigifiedCard.push_back('/');
		strinigifiedCard+=std::to_string(_karty[id]._specjalne.zmniejszKosztZaSymbol.second);
	}
	strinigifiedCard.push_back(';');
	strinigifiedCard.push_back('\n');



	return strinigifiedCard;
}

std::string Deck::toString()
{
	std::string strinigifiedDeck;
	std::string lastCardString;

	for(auto it : _karty)
	{
		lastCardString=toString(it.first);
		strinigifiedDeck.append(lastCardString);
	}
	return strinigifiedDeck;
}

void Deck::toJson(json& j,int id)
{
	j["id"]=_karty[id]._id;
/*	j["cena"]=_karty[id]._cena;
	j["karty"]=_karty[id]._karty;
	j["zwyciestwo"]=_karty[id]._zwyciestwo;
	j["symbole"]=_karty[id]._symbole;
	j["unikat"]=_karty[id]._unikat;
	j["limit"]=_karty[id]._limit;
	j["specjalne"]=_karty[id]._specjalne;
*/
}

void Deck::toJson(json& all)
{
	json j;
	std::cout<<"hel";

	for(auto it:_karty)
	{
		j.clear();
		j["id"]=it.second._id;
		/*j["cena"]=it.second._cena;
		j["karty"]=it.second._karty;
		j["zwyciestwo"]=it.second._zwyciestwo;
		j["symbole"]=it.second._symbole;
		j["unikat"]=it.second._unikat;
		j["limit"]=it.second._limit;
		j["specjalne"]=it.second._specjalne;
		*/
		all+=j;
	}
}

void Deck::parsePoints(Points* points, std::string karty)
{
	points->baza=0;
	points->budynek=std::pair<int,int>(0,-1);
	points->budynekwGrze=std::pair<int,int>(0,-1);
	points->symbol=std::pair<int,int>(0,-1);
	points->symbolPrzeciwnik=std::pair<int,int>(0,-1);

	std::string temp1,temp2;
	/*
	 *  0 - baza
	 *  1- symbole
	 *  2- budynk
	 *  3- budynek w grze
	 *  4- symbol przeciwnik
	*/
	int typ=0;
	int first =0;
	temp1.clear();
	temp2.clear();

	for(int i=0;i<karty.size();++i)
	{
		switch(karty.at(i))
		{
			case '*':
				typ =2;
				first=1;
				break;
			case '^':
				typ=3;
				first=1;
				break;
			case '/':
				typ=1;
				first=1;
				break;
			case '+':
				first=0;
				switch(typ)
				{
				case 0:
					std::istringstream(temp1)>>points->baza;
					break;
				case 1:
					{
						int pkt;
						int sym;
						std::istringstream(temp1)>>pkt;
						switch(temp2.at(0))
						{
						case 'K':
							sym=Symbol::Kultura;
						break;
						case 'H':
							sym=Symbol::Handel;
						break;
						case 'T':
							sym=Symbol::Transport;
						break;
						}
						points->symbol = std::pair<int,int>(pkt,sym);
					}
					break;
				case 2:
					{
						int pkt;
						int sym;
						std::istringstream(temp1)>>pkt;
						std::istringstream(temp2)>>sym;
						points->budynek = std::pair<int,int>(pkt,sym);
					}
					break;
				case 3:
					{
						int pkt;
						int sym;
						std::istringstream(temp1)>>pkt;
						std::istringstream(temp2)>>sym;
						points->budynekwGrze = std::pair<int,int>(pkt,sym);
					}
					break;
				case 4:
					{
						int pkt;
						int sym;
						std::istringstream(temp1)>>pkt;
						std::istringstream(temp2)>>sym;
						points->symbolPrzeciwnik = std::pair<int,int>(pkt,sym);
					}
					break;
				}
				temp1.clear();
				temp2.clear();
				break;
			case '@':
				first=1;
				typ=4;
				break;
			default:
				if(first==0)
				{
					temp1+=karty.at(i);
				}else
				{
					temp2+=karty.at(i);
				}
		}
	}
	switch(typ)
	{
	case 0:
		std::istringstream(temp1)>>points->baza;
		break;
	case 1:
		{
			int pkt;
			int sym;
			std::istringstream(temp1)>>pkt;
			switch(temp2.at(0))
			{
			case 'K':
				sym=Symbol::Kultura;
			break;
			case 'H':
				sym=Symbol::Handel;
			break;
			case 'T':
				sym=Symbol::Transport;
			break;
			}
			points->symbol = std::pair<int,int>(pkt,sym);
		}
		break;
	case 2:
		{
			int pkt;
			int sym;
			std::istringstream(temp1)>>pkt;
			std::istringstream(temp2)>>sym;
			points->budynek = std::pair<int,int>(pkt,sym);
		}
		break;
	case 3:
		{
			int pkt;
			int sym;
			std::istringstream(temp1)>>pkt;
			std::istringstream(temp2)>>sym;
			points->budynekwGrze = std::pair<int,int>(pkt,sym);
		}
		break;
	case 4:
		{
			int pkt;
			int sym;
			std::istringstream(temp1)>>pkt;
			std::istringstream(temp2)>>sym;
			points->symbolPrzeciwnik = std::pair<int,int>(pkt,sym);
		}
		break;
	}
	temp1.clear();
	temp2.clear();
}






